from Dataset import Dataset
import random
import progressbar

epsilon = 100
theta = random.random()
alpha = 0.001

def hypothesis(x):
    global theta
    return x*theta

def loss(X,Y):
    sum = 0
    for i in range(0,len(X)):
        sum += (hypothesis(X[i]) - Y[i])**2
    
    return (1/(2*len(X)))*sum

def partial_differenciation(X,Y):
    sum = 0
    for i in range(0,len(X)):
        sum += (hypothesis(X[i]) - Y[i])*X[i]
    return float((1/len(X))*sum)

def train(X,Y):
    diff = partial_differenciation(X,Y)
    sub_loss = loss(X,Y)
    print("Loss = "+str(sub_loss))
    i = 0
    while(i < 100):
        global theta
        theta = theta - alpha*diff
        diff = partial_differenciation(X,Y)
        if (i%10 == 0):
            sub_loss = loss(X,Y)
            print("Loss = "+str(sub_loss))
        i += 1

def crossValidation(X,Y):
    print("Theta = "+str(theta))
    return loss(X,Y)

def test(X,Y):
    tp = 0
    bar = progressbar.ProgressBar(maxval=len(X), \
                widgets=[progressbar.Bar('=', '[', ']'), ' ', progressbar.Percentage()])
    print("Testing...")

    bar.start()
    for i in range(0,len(X)):
        if(hypothesis(X[i]) - Y[i] > epsilon):
            tp += 1
        bar.update(i)
    bar.finish()
    return tp/len(X)

if __name__ == "__main__":
    dataset = Dataset("bottle.csv")
    dataset.createDatasets()

    train_set = dataset.getTrainingSet()
    cv_set = dataset.getCrossValidationSet()
    test_set = dataset.getTestSet()

    print("Training the Model.")
    train(train_set[0],train_set[1])
    cv_loss = crossValidation(cv_set[0],cv_set[1])
    print("Loss in Cross Validation = "+str(cv_loss))
    test_accuracy = test(test_set[0],test_set[1])
    print("Accuracy of Model on Test set = "+str(test_accuracy*100)+"%")
