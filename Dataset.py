import csv
import time
from random import shuffle


class Dataset:
    filename = ""
    count = 0
    salinity = []
    temperature = []

    
    def __init__(self,filename):
        self.filename = filename

    def createDatasets(self):
        print("Opening CSV file: "+self.filename)
        t0 = time.time()
        with open(self.filename) as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            self.cv_s = int(self.count*0.6 + 1)
            self.test_s = int(self.cv_s + self.count*0.2 + 1)

            print("File opened in "+str(time.time() - t0)+" seconds.\n\nCreating Datasets.")

            for row in reader:
                try:
                    if(row[6].strip() != "" and row[5].strip() != ""):
                        self.salinity.append(float(row[6]))
                        self.temperature.append(float(row[5]))
                except ValueError:
                    print("Salinity: "+row[6]+" Temp: "+row[5])

            self.count = len(self.salinity)
            t = [self.salinity, self.temperature]
            shuffle(t)
            self.salinity = t[0]
            self.temperature = t[1]
            print("Total count = "+str(self.count))

                

    def getTrainingSet(self):
        return [self.salinity[0:int(self.count*0.6)],self.temperature[0:int(self.count*0.6)]]

    def getCrossValidationSet(self):
        return [self.salinity[int(self.count*0.6):int(self.count*0.6+self.count*0.2)],self.temperature[int(self.count*0.6):int(self.count*0.6+self.count*0.2)]]

    def getTestSet(self):
        return [self.salinity[int(self.count*0.6+self.count*0.2):self.count],self.temperature[int(self.count*0.6+self.count*0.2):self.count]]
    

        